[Playground] AWS Lambda Functions
===
### Informações de Suporte e limitações técnicas

**Linguanges**
 - Node.js
 - C#
 - Python
 - Java

**Max Executation Time:** 5 minutos
**Memory:** 128 MB - 1536 MB
**CPU Allocation:** Depende da quantidade de memória allocada
**Concurrent Executation Limit (default 1000) & Safely Limit**
**Event Sources**
  - S3
  - DynamoDB
  - Kinesis Streams
  - Simple Notification Service
  - Simple Email Service
  - Cognito
  - CloudFormation (infra as a Code)
  - CloudWatch (Logger)
  - CodeCommit (git)
  - Scheduled Events
  - Config
  - Alexa
  - Lex
  - API Gateway