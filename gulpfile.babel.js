import gulp from 'gulp'
import zip from 'gulp-zip'
import babel from 'gulp-babel'
import color from 'gulp-color'
import install from 'gulp-install'
import runSequence from 'run-sequence'
import del from 'del'
import fs from 'fs'
import * as t from 'child_process'

// --- Helpers
const log = (mensagem, rgb) => console.log(color(mensagem, rgb || 'RED'))

const isDirectory = (dirPath) =>  {
  try{
    return fs.lstatSync(dirPath).isDirectory()
  }catch (err) {
    return false
  }
}

const getArg = (option) => {
  const i = process.argv.indexOf(`--${option}`)
  return  (i > -1)? process.argv[i + 1] : false | log(`[Paramenter Error] - option "${option}" inválida!`)
}

const hasArg = (option) => (process.argv.indexOf(`--${option}`) > -1)

const validaVariaveis = () => {

  const lambda = getArg('lambda')

  if (!lambda) {
    log('o parametro "--lambda" deve ser informado (ex: gulp <task> --lambda HelloWord )')
    process.exit(1)
    return false
  }

  const lambdaPath = `${__dirname}/src/functions/${lambda}`

  if (!isDirectory(lambdaPath)) {
    log(`[Erro:zip]
      Path não encontrado, verifique se o seu diretório esta correto:
          --lambda
            - "${lambda}"
          Local da sua function
            - "${lambdaPath}"
    `)
    return false
  }

  return true
}
// --- FIM - Helpers

gulp.task('default', () => {
  log(`
  Comandos:
  - 'gulp clean'
  - 'gulp build --lambda <hello-world>'
  - 'gulp zip --lambda <hello-world>'
  - 'gulp deploy --lambda <hello-world>'
  - 'gulp sam-local-invoke --lambda <hello-world>'

  --lambda <- O parametro "lambda" deve ser o nome do folder onde a lambda
              (EX:  hello-world => "./src/functions/hello-world")
  `)
})

gulp.task('clean', function(cb) {
  if (hasArg('skip-node_modules')) {
    return del(['./dist/index.js'], cb)
  } else return del(['./dist'], cb)
})

gulp.task('deploy',['zip'], () => {
  const lambda = getArg('lambda')

  t.exec(`aws s3 cp ./pre-deploy/${lambda}.zip  s3://br.com.lleitep3/lambda/`, (err, stdout) => {
    log(stdout)
  })
})

gulp.task('sam-local-invoke',['build'], () => {

  t.exec('sam local invoke -t ./dist/template.yaml -e ./dist/event.json', (err, stdout, stderr) => {
    if (stderr) {
      log(stderr)
    }

    log('------------- OUTPUT -------------')
    log(stdout)
    log('------------- OUTPUT -------------')
  })

})

gulp.task('zip', ['build'], () => {
  const lambda = getArg('lambda')

  return gulp.src('./dist/**/*')
    .pipe(zip(`${lambda}.zip`))
    .pipe(gulp.dest('pre-deploy'))
})

gulp.task('assets', () => {
  const lambda = getArg('lambda')
  const lambdaPath = `${__dirname}/src/functions/${lambda}`

  return gulp
    .src([`${lambdaPath}/**/*`, `!${lambdaPath}/**/*.js`])
    .pipe(gulp.dest('dist'))
})

gulp.task('transpileJS', () => {
  const lambda = getArg('lambda')
  const lambdaPath = `${__dirname}/src/functions/${lambda}`

  return gulp
    .src([`${lambdaPath}/**/*.js`])
    .pipe(babel({ plugins: [
      'transform-es2015-modules-commonjs',
      'transform-object-rest-spread',
      'babel-plugin-transform-async-to-generator'
    ]}))
    .pipe(gulp.dest('dist'))
})

gulp.task('transpileSharedJS', () => {

  return gulp.src(['./src/shared/**/*.js'])
    .pipe(babel({ plugins: [
      'transform-es2015-modules-commonjs',
      'transform-object-rest-spread',
      'babel-plugin-transform-async-to-generator'
    ]}))
    .pipe(gulp.dest('dist/shared'))
})

gulp.task('node-mods', function() {
  return hasArg('skip-node_modules')? true : gulp.src('./package.json')
    .pipe(gulp.dest('dist/'))
    .pipe(install({production: true}))
})

gulp.task('build', ['clean'], (callback) => {
  if (!validaVariaveis()) {
    process.exit()
    return
  }

  return runSequence(
    ['transpileJS'],
    ['transpileSharedJS'],
    ['assets'],
    ['node-mods'],
    callback
  )
})

gulp.task('create-lambda', () => {
  const name = getArg('name')
  if (!name) {
    log('A Opção "--name" é obrigatória')
    return
  }

  t.exec(`
  mkdir ./src/functions/${name}/ &&
  touch ./src/functions/${name}/event.json &&
echo "'use strict'

// A simple hello world Lambda function
exports.handler = (event, context, callback) => {

  const result = concatenar(Object.values(event))
  callback(null, result)
}

function concatenar(...text) {
  return text.join(' , ')
}" > ./src/functions/${name}/index.js &&
  echo "AWSTemplateFormatVersion: '2010-09-09'
Transform: AWS::Serverless-2016-10-31
Description: A simple Hello World Serverless project
Resources:
  ${name}:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: nodejs6.10
      Handler: index.handler
      CodeUri: .
" > ./src/functions/${name}/template.yaml
  `)
})

gulp.task('watch', () => {
  const lambda = getArg('lambda')
  gulp.watch(`./src/functions/${lambda}/index.js`, ['transpileJS'])
  gulp.watch([`./src/functions/${lambda}/template.yaml`,`./src/functions/${lambda}/event.json`], ['assets'])
})

gulp.task('pack', ['build'], () => {
  if (!validaVariaveis()) {
    return
  }
  log('Efetuando upload...')
  return t.exec('sam package --template-file ./dist/template.yaml --s3-bucket br.com.lleitep3 --output-template-file ./dist/packaged.yaml', (err, stdout) => {
    log(stdout)
  })
})

gulp.task('deploy', () => {
  return t.exec('aws cloudformation deploy --template-file /home/heyman/Documents/projects/sam-tests/dist/packaged.yaml --stack-name api --capabilities CAPABILITY_IAM', (err, stdout) => {
    log(stdout)
  })
})