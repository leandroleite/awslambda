import {GenericError, TimeoutError} from './Errors'

const ERROR_MESSAGE_TIMEOUT = 'Timeout ocorrido antes de executar`'

class Runner {

  constructor(event, context, callback) {
    this.event = event
    this.context = context
    this.callback = callback
  }

  async run(work, compensation) {
    try {
      if (!weHaveTime(this.event.infra.deadline))
        throw new TimeoutError(`'[${this.context.functionName}]: ${ERROR_MESSAGE_TIMEOUT}`)

      const result = await work(this.event, this.context).timeout(this.event.infra.deadline)

      this.callback(null, result)
      return result
    }
    catch (error) {
      await compensation(error, this.event, this.context)
      this.callback(new GenericError(`Erro ao executar a lambda function '${this.context.functionName}'`, error))
    }
  }

}

const weHaveTime = (deadline) => (deadline - new Date().getTime())

export default Runner