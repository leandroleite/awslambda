import NestedError from 'nested-error-stacks'

export default class GenericError extends NestedError {
  constructor(message, nested) {
    GenericError.prototype.name = 'GenericError'
    super(message, nested)
  }
}

export class TimeoutError extends GenericError {
  constructor(message, nested) {
    TimeoutError.prototype.name = 'TimeoutError'
    super(message, nested)
  }
}

export class InvokerError extends GenericError {
  constructor(message, nested) {
    InvokerError.prototype.name = 'InvokerError'
    super(message, nested)
  }
}