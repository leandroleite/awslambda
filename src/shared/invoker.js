import AWS from 'aws-sdk'
import InvokerError from './Errors'

AWS.config.update({
  region: 'us-west-2',
  lambda: '2015-03-31'
})

export default async function invoker (functionName, event) {
  const params = {
    FunctionName: functionName,
    Payload: JSON.stringify(event)
  }

  try {

    const lambda = new AWS.Lambda()
    const lambdaInvoke = Promise.promisify(lambda.invoke, {context: lambda})
    return await lambdaInvoke(params)

  } catch(err) {
    throw new InvokerError(`Ocorreu um erro ao tentar invokar a lambda "${functionName}"`, err)
  }
}
