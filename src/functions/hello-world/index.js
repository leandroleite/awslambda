import Runner from './shared/Runner'
import Promise from 'bluebird'
import lambda from './lambda'

global.Promise = Promise

const compensation = require('./lambda').compensation || ((err) => {console.log(err)})

exports.handler = async (event, context, callback) =>
  (await (new Runner(event, context, callback)))
    .run(lambda, compensation)
