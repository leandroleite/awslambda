import invoker from './shared/invoker'

const providerFunctionName = 'errorFunction'

export default async (event, context) => {
  const result = await invoker(providerFunctionName, {event, context})

  let response = { statusCode : 200 }

  if (isResponseError(result)) response.statusCode = 500

  return Object.assign(response, {
    body: result.Payload
  })
}

const isResponseError = (response) => JSON.parse(response.Payload).errorType
