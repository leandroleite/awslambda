import Runner from './shared/runner'
import lambda from './lambda'
import Promise from 'bluebird'

global.Promise = Promise

const onError = require('./lambda').compensation || ((err) => {console.log(err)})

exports.handler = async (event, context, callback) =>
  (await (new Runner(event, context, callback)))
    .run(lambda)
    .catch(onError)
