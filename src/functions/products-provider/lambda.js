
export default async (event) => {

  if (event.timeout === true) {
    await Promise.delay(3000)
  }

  if (event.dispatchError === true) {
    throw new Error('[Product Provider][Error] - Erro Disparado')
  }

  return [
    {nome: 'Produto 1'},
    {nome: 'Produto 2'},
    {nome: 'Produto 3'},
  ]
}

export const compensation = async () => {
  // aqui compensation
  console.log('[ProductProvider] - Compensation Executado')
  await Promise.delay(3000)
  return true
}